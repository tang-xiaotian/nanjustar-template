package com.nanjustar.common.constant;

public class SystemConst {

    /**
     * 用户登陆成功
     */
    public static final String LOGIN_SUCCESS = "用户登陆成功！";

    /**
     * 用户登陆失败
     */
    public static final String LOGIN_FAIL = "用户名或密码错误！";

    /**
     * 用户退出成功
     */
    public static final String LOGOUT_SUCCESS = "用户退出成功！";


}
