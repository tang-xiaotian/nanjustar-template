package com.nanjustar.common.constant;


public class CommonConst {

    /**
     * 菜单路由父级id
     */
    public static final Integer MENU_LAYOUT_ID = -1;

    /**
     * 禁用状态
     */
    public static final Integer DISABLE = 1;

    /**
     * 正常状态
     */
    public static final Integer NOT_DISABLE = 0;

    /**
     * 隐藏
     */
    public static final Integer HIDDEN = 1;

    /**
     * 显示
     */
    public static final Integer NOT_HIDDEN = 0;

    /**
     * 删除
     */
    public static final Integer DELETE = 1;

    /**
     * 未删除
     */
    public static final Integer NOT_DELETE = 0;
}
