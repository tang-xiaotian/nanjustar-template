package com.nanjustar.mapper.mapper.system;

import com.nanjustar.api.moudle.system.entity.OptLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author nanjustar
 * @since 2021-11-13
 */
public interface OptLogMapper extends BaseMapper<OptLog> {

}
