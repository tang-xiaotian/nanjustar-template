package com.nanjustar.api.moudle.system.api;

import com.nanjustar.api.moudle.system.entity.OptLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author nanjustar
 * @since 2021-11-13
 */
public interface OptLogService extends IService<OptLog> {

}
